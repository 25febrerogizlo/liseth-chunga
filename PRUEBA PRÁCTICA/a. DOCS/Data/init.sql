CREATE DATABASE IF NOT EXISTS gizlo;

USE gizlo;

DROP TABLE IF EXISTS authorities;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS person;
DROP TABLE IF EXISTS product;

CREATE TABLE product (
                         id integer PRIMARY KEY NOT NULL,
                         title varchar(120) NOT NULL,
                         price decimal(10,2) NOT NULL,
                         description varchar(1500) NOT NULL,
                         category varchar(50) NOT NULL,
                         image varchar(120) NOT NULL
);

CREATE TABLE person (
                        id varchar(36) PRIMARY KEY NOT NULL,
                        identification_number varchar(10) NOT NULL,
                        names varchar(50) NOT NULL,
                        surnames varchar(50) NOT NULL,
                        email varchar(40) NOT NULL,
                        CONSTRAINT UK_PERSON_IDENTIFICATION_NUMBER UNIQUE (identification_number)
);

CREATE TABLE users (
                       id varchar(36) PRIMARY KEY NOT NULL,
                       username varchar(25) NOT NULL,
                       password varchar(100) NOT NULL,
                       person_id varchar(36),
                       CONSTRAINT UK_USER_USERNAME UNIQUE (username),
                       CONSTRAINT FK_USER_PERSON FOREIGN KEY (person_id) REFERENCES person(id)
);


CREATE TABLE authorities (
                             user_id varchar(36) NOT NULL,
                             role varchar(50) NOT NULL,
                             CONSTRAINT FK_USER_ROLES FOREIGN KEY (user_id) REFERENCES users(id)
);



INSERT INTO person (id, identification_number, names, surnames, email) VALUES
    ('a8b8de8f-17df-4d14-9083-d419324bbb95', '0929363547', 'Liseth Melina', 'Chunga Bayas', 'lmchungab@hotmail.com');
INSERT INTO users (id, username, password, person_id) VALUES
    ('23fb3057-2962-44e8-b7e4-3d1532b3f9d6', 'root', '$2a$10$yxWDeJgc0zW7n42n6w6gqO0rfLV8goT4.1eURCKuuAwXb12rFOlxa', 'a8b8de8f-17df-4d14-9083-d419324bbb95');
INSERT INTO authorities (user_id, role) VALUES
    ('23fb3057-2962-44e8-b7e4-3d1532b3f9d6', 'ROLE_ADMIN');