# Gizlo Test

Tienda virtual.

## Construcción

Sistema construido con Java 11 y Spring Boot 2.6.7.  
El sistema cuenta con la implementación del estándar JWT.  
La documentación de la API se realizó con Swagger 2.9.2.  
Se usó mysql como gestor de base de datos.  
Se encuentra adjunto el script inicial de la base de datos, el modelo de datos y una colección en postman para probar la API.

## Despliegue
Se deben cambiar las credenciales de conexion a base de datos dentro del archivo application.properties.  
Ejecutar el comando mvn spring-boot:run

## Indicaciones
El nombre de usuario del administrador es *root* y la contraseña es *toor*.  
Dentro de la colección de postman se encuentran ejemplos de la ejecución para poder probar el funcionamiento del sistema.  
