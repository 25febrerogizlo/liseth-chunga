package com.gizlo.backend.controller;

import com.gizlo.backend.dto.filter.ProductFilter;
import com.gizlo.backend.entity.Product;
import com.gizlo.backend.service.ProductService;
import com.gizlo.backend.util.ResponseEntityUtils;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

import static com.gizlo.backend.common.Constants.URI_PRODUCT;


@RestController
@RequestMapping(value = {URI_PRODUCT})
@Api(value = "Products Management", tags = "product")
public class ProductController {

    private final Logger LOG = LoggerFactory.getLogger(ProductController.class);

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("verifyData")
    public boolean verifyData() {
        try {
            return productService.verifyProductData();
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            return false;
        }
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("loadData")
    public void loadData() {
        try {
            productService.loadProductData();
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
        }
    }

    @GetMapping("find/{productId}")
    public ResponseEntity<Product> findOne(@PathVariable("productId") Integer productId) {
        try {
            Optional<Product> product = productService.findOne(productId);
            return product
                    .map(value -> new ResponseEntity<>(value, HttpStatus.OK))
                    .orElseGet(() -> new ResponseEntity<>(null, HttpStatus.BAD_REQUEST));
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            return ResponseEntity
                    .badRequest()
                    .headers(ResponseEntityUtils.generateErrorHeaders(Product.class, ex))
                    .body(null);
        }
    }

    @PostMapping("filter")
    public ResponseEntity<Collection<Product>> filter(@RequestBody ProductFilter filter) {
        try {
            Collection<Product> products = productService.filterProducts(filter);
            return new ResponseEntity<>(products, HttpStatus.OK);
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            return ResponseEntity
                    .badRequest()
                    .headers(ResponseEntityUtils.generateErrorHeaders(Product.class, ex))
                    .body(null);
        }
    }

    @PostMapping("page")
    public ResponseEntity<Page<Product>> page(@RequestBody ProductFilter filter) {
        try {
            Page<Product> products = productService.pageProducts(filter);
            return new ResponseEntity<>(products, HttpStatus.OK);
        } catch (Exception ex) {
            LOG.error(ex.getMessage());
            return ResponseEntity
                    .badRequest()
                    .headers(ResponseEntityUtils.generateErrorHeaders(Product.class, ex))
                    .body(null);
        }
    }

}
