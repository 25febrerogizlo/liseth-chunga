package com.gizlo.backend.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.gizlo.backend.dto.filter.ProductFilter;
import com.gizlo.backend.entity.Product;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.Optional;

public interface ProductService {

    boolean verifyProductData() throws JsonProcessingException;
    void loadProductData() throws JsonProcessingException;
    Optional<Product> findOne(Integer productId);
    Collection<Product> filterProducts(ProductFilter filter);
    Page<Product> pageProducts(ProductFilter filter);

}
