package com.gizlo.backend.entity;

import com.gizlo.backend.util.StringUtils;
import lombok.*;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.*;
import javax.validation.constraints.*;

@Getter
@Setter
@Entity
@Table(name="person", uniqueConstraints = {
        @UniqueConstraint(name = "UK_PERSON_IDENTIFICATION_NUMBER", columnNames = {"identification_number"})
})
@NoArgsConstructor
@AllArgsConstructor
public class Person extends AbstractEntity {

    @NotEmpty
    @Size(min = 10, max = 10)
    @Pattern(regexp="^[0-9]{10}$", message="This field only accepts numeric values")
    @Column(name="identification_number", length = 10)
    private String identificationNumber;

    @NotEmpty
    @Pattern(regexp="^[A-Za-z_ ]*$", message="This field only accepts letters")
    @Column(name="names", length = 50)
    private String names;

    @NotEmpty
    @Pattern(regexp="^[A-Za-z_ ]*$", message="This field only accepts letters")
    @Column(name="surnames", length = 50)
    private String surnames;

    @NotEmpty
    @Email
    @Column(name="email", length = 40)
    private String email;

     public String getFullName() {
        return concatNames(names, surnames);
    }

    public String getFullNameInverse() {
        return concatNames(surnames, names);
    }

    private String concatNames(String... names) {
        return Stream.of(names)
                .map(StringUtils::trimToNull)
                .filter(Objects::nonNull)
                .collect(Collectors.joining(" "));
    }

}
