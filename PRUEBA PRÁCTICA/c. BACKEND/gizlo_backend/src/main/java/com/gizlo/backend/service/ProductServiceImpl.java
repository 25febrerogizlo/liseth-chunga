package com.gizlo.backend.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gizlo.backend.dto.filter.ProductFilter;
import com.gizlo.backend.entity.Product;
import com.gizlo.backend.repository.ProductRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
//@Transactional
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    @Override
    public boolean verifyProductData() {
        long quantity = productRepository.count();
        return quantity > 0;
    }

    @Override
    public void loadProductData() throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity("https://fakestoreapi.com/products", String.class);
        String body = response.getBody();

        ObjectMapper mapper = new ObjectMapper();
        Product[] products = mapper.readValue(body, Product[].class);

        Collection<Product> productsCollection = new ArrayList<>(List.of(products));

        productRepository.saveAll(productsCollection);
    }

    @Override
    public Optional<Product> findOne(Integer productId) {
        return productRepository.findById(productId);
    }

    @Override
    public Collection<Product> filterProducts(ProductFilter filter) {
        Assert.notNull(filter, "Filters are required");

        return productRepository.listBySearchTerm(filter.getSearchTerm());
    }

    @Override
    public Page<Product> pageProducts(ProductFilter filter) {
        Assert.notNull(filter, "Filters are required");
        Assert.notNull(filter.getPage(), "Page number is required");
        Assert.notNull(filter.getSize(), "Page size is required");

        Pageable pageable = PageRequest.of(filter.getPage(), filter.getSize(), Sort.by("title"));

        return productRepository.pageBySearchTerm(filter.getSearchTerm(), pageable);
    }
}
