package com.gizlo.backend.enums;

public enum ResponseHeader {
    ERROR_MESSAGE,
    ENTITY,
    ADDITIONAL_INFO
}