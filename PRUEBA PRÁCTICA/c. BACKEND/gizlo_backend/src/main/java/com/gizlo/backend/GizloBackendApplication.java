package com.gizlo.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GizloBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(GizloBackendApplication.class, args);
    }

}
