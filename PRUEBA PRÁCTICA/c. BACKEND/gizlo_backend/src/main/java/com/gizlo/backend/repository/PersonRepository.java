package com.gizlo.backend.repository;

import com.gizlo.backend.entity.Person;

import java.util.Optional;

public interface PersonRepository extends AbstractEntityRepository<Person> {

    Optional<Person> findByIdentificationNumber(String identificationNumber);

}
