package com.gizlo.backend.repository;

import com.gizlo.backend.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Query("SELECT p FROM Product p " +
            "WHERE LOWER(p.title) LIKE %:searchTerm% OR " +
            "LOWER(p.description) LIKE %:searchTerm% OR " +
            "LOWER(p.category) LIKE %:searchTerm%")
    Collection<Product> listBySearchTerm(@Param("searchTerm") String searchTerm);
    @Query("SELECT p FROM Product p " +
            "WHERE LOWER(p.title) LIKE %:searchTerm% OR " +
            "LOWER(p.description) LIKE %:searchTerm% OR " +
            "LOWER(p.category) LIKE %:searchTerm%")
    Page<Product> pageBySearchTerm(@Param("searchTerm") String searchTerm, Pageable pageable);
}
