package com.gizlo.backend.service;


import com.gizlo.backend.entity.User;

import java.util.Optional;

public interface UserService {

    Optional<User> getByUsername(String username);
    boolean existsByUsername(String username);
    void save(User user);

}
