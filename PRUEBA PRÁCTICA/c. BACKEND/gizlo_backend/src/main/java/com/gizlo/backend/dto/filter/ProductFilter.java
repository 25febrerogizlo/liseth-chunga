package com.gizlo.backend.dto.filter;

import lombok.Getter;

@Getter
public class ProductFilter extends PageFilter {
    String searchTerm;
}
