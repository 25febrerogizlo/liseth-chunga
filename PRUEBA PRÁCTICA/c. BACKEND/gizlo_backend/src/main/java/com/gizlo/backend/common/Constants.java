package com.gizlo.backend.common;

public class Constants {

    public static final String APP_DOMAIN = "localhost";

    // API URI
    private static final String URI_API = "/api/";

    public static final String URI_AUTH = URI_API + "auth";
    public static final String URI_PRODUCT = URI_API + "product";

    // USER PROPERTIES
    public static final String CURRENT_USER = "CURRENT_USER";
    public static final String CURRENT_USERNAME = "CURRENT_USERNAME";
    public static final String ROLES = "ROLES";
}
