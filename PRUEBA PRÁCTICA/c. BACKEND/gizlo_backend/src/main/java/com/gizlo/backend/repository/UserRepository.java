package com.gizlo.backend.repository;

import com.gizlo.backend.entity.User;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends AbstractEntityRepository<User> {

    Optional<User> findByUsername(String username);
    boolean existsByUsername(String username);
//    List<User> findByPersonIdIn(List<UUID> ids);
//    Short countAllBy();

}
