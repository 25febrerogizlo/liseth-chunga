package com.gizlo.backend.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@Setter
@Entity
@Table(name="product")
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {

    @Id
    @Column(name="id", nullable = false, updatable = false)
    private Integer id;

    @NotEmpty
    @Size(max = 120)
    @Column(name="title", length = 10)
    private String title;

    @Column(name="price")
    @Min(0)
    private Double price;

    @NotEmpty
    @Column(name="description", length = 1500)
    private String description;

    @NotEmpty
    @Column(name="category", length = 50)
    private String category;

    @NotEmpty
    @Column(name="image", length = 120)
    private String image;

}
