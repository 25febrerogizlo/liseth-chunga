package com.example.gizlo_alg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.security.SecureRandom;
import java.util.Random;
import java.util.Scanner;
import java.util.*;


@SpringBootApplication
public class GizloAlgApplication {

    public static void main(String[] args) {
        SpringApplication.run(GizloAlgApplication.class, args);

        int option = 0;

        do {
            System.out.println("Presione 1 para ingresar al algoritmo 1(String al revés)");
            System.out.println("Presione 2 para ingresar al algoritmo 2(Clave Random");
            System.out.println("Presione 3 para finalizar");
            System.out.println("\n");
            System.out.println("Ingrese: ");
            Scanner sc = new Scanner(System.in);
            option = sc.nextInt();
            switch (option){
                case 1:
                    firstAlgorithm();
                    break;
                case 2:
                    secondAlgorithm();
                    break;
                case 3:

                    break;
                default:
                    System.out.println("Ingrese solamente las opciones 1, 2 o 3");
                    break;
            }
        } while (option != 3);

    }

    private static void firstAlgorithm() {
        Scanner scanner = new Scanner(System.in);

        // Pedimos al usuario que ingrese el string a reordenar
        System.out.println("Ingrese la frase que va a ser reordenada:");
        String input = scanner.nextLine();

        // Pedimos al usuario que ingrese la palabra que se usará para reemplazar los espacios
        System.out.println("Ingrese la palabra que reemplazará los espacios:");
        String replacement = scanner.nextLine();

        // Separamos el string en palabras usando los espacios como separadores
        String[] words = input.split(" ");
        String reversedPhrase = "";
        // Reordenamos las palabras de adelante hacia atrás
        for (int i = words.length - 1; i >= 0; i--) {
            String reversedWord = "";
            //convirtiendo el char a array(palabra)
            char[] wordarray = words[i].toCharArray();
            for (int j = wordarray.length -1; j>=0; j--){
                reversedWord += wordarray[j];
            }
            reversedPhrase+=reversedWord+replacement;
        }

        // Eliminamos la última palabra de reemplazo que no queremos
        reversedPhrase = reversedPhrase.substring(0, reversedPhrase.length() - replacement.length());

        // Mostramos el string reordenado
        System.out.println("\n");
        System.out.println("String reordered: " + reversedPhrase);
    }

    private static void secondAlgorithm() {
        // Generamos una clave aleatoria de longitud entre 8 y 15 caracteres
        Random randomPass = new Random();
        int lengthPass = randomPass.nextInt(8) + 8;
        char[] code = new char[lengthPass];

        // Aseguramos que la clave cumpla con los requisitos especificados
        boolean hasUpper = false;
        boolean hasLower = false;
        boolean hasSpecialChar = false;
        while (!hasUpper || !hasLower || !hasSpecialChar) {
            for (int i = 0; i < lengthPass; i++) {
                char currentChar;
                int charType = randomPass.nextInt(3);
                switch (charType) {
                    case 0 -> {
                        currentChar = (char) (randomPass.nextInt(26) + 'A');
                        hasUpper = true;
                    }
                    case 1 -> {
                        currentChar = (char) (randomPass.nextInt(26) + 'a');
                        hasLower = true;
                    }
                    case 2 -> {
                        currentChar = getRandomSpecialCharacter();
                        hasSpecialChar = true;
                    }
                    default -> throw new IllegalStateException("Invalid character type");
                }
                code[i] = currentChar;
            }
        }
        System.out.println("\n");
        System.out.println("Su código Random es: "+new String(code));
    }

    private static char getRandomSpecialCharacter() {
        String specialCharacters = "!@#$%^&*()_+-=";
        int index = new Random().nextInt(specialCharacters.length());
        return specialCharacters.charAt(index);
    }
}
